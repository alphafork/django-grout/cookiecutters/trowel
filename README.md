# Trowel - Django App Template

[Cookiecutter](https://github.com/cookiecutter/cookiecutter) template for
bootstrapping a reusable Django app based on the
[django-grout](https://gitlab.com/alphafork/django-grout) meta framework.


## Quick Start

Install `cookiecutter` and `jinja2_strcase` (used for converting string case) if not already installed:

`pip install --user cookiecutter jinja2_strcase`

Generate Django app using the Trowel cookiecutter:

`cookiecutter https://gitlab.com/alphafork/django-grout/cookiecutters/trowel`


## License

[GPL-3.0-or-later](LICENSE)


## Contact

[Alpha Fork Technologies](https://alphafork.com)

Email: [connect@alphafork.com](mailto:connect@alphafork.com)
