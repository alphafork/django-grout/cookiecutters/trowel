# {{ cookiecutter.project_name }}

{{ cookiecutter.project_short_description }}


## License

{% if cookiecutter.license == "AGPL-3.0-or-later" %}
  [AGPL-3.0-or-later](LICENSE)
{% else %}
  {{ cookiecutter.license }}
{% endif %}


## Contact

{{ cookiecutter.full_name }}

[{{ cookiecutter.email }}](mailto:{{ cookiecutter.email }})
