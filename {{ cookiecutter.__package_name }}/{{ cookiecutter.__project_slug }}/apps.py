from django.apps import AppConfig


class {{ cookiecutter.project_name | to_camel }}Config(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "{{ cookiecutter.__project_slug }}"
